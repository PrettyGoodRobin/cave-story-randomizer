Cave Story Randomizer
=====================

Todo
----

- Trade Sequence Step B: Require random obtainable weapon, instead of always polar star and fireball.
- Randomize Booster v0.8 / v2.0

Issues
------

- Infinite Items at Chaco's?!?!
- [Speedrun] When missiles are replaced, sometimes text will exceed message box after "Opened the chest." text. (ie. https://youtu.be/PYhd9zhFdAk?t=4769)
- Collecting the Super Missile Launcher increases your maximum missiles by 5. This does not normally happen.
- Hell Missile Upgrade uses a unique script and won't be easy to replace.
- Trading back the Nemesis for the Blade almost certainly will be weird.
- Ikachan Medal aquisition happens in black screen, so it doesn't make sense to show text box as it makes the player have to mash through blackness.

Bugs
----
- Jenka gave me a life pot, but since I already had one, nothing happened. It even stayed in the same spot, instead of moving to the front.

Credits
-------

- Font: https://datagoblin.itch.io/monogram
